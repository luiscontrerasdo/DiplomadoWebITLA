/* Declaramos una serie de variables */

var estudiantes = [];
var storage = window.localStorage;

/* Definimos una funcion llamada cargandoDatos() a los fines de cargar los datos */

window.onload = function cargandoDatos() 
{
    if (typeof (storage) !== "undefined") {
       if (storage.length > 0) {
            estudiantes = JSON.parse(storage.getItem("estudiantes"));
            cargandoTabla(estudiantes);
        }
    } else {
        alert("No se soportado");
        }

}

/* Fin de la definicion de la funcion cargandoDatos() */

/* Definimos la funcion leerEstudiante() */

function leerEstudiante() {
   
    // Definimos las variables del formulario
    var nombre = document.getElementById("nombre").value;
    if (nombre == "") {
        alert("Campo nombre esta vacio");
        return false;
    }
    var matricula = document.getElementById("matricula").value;
    if (matricula == "") {
        alert("Campo matricula esta vacio");
        return false;
    }
    var identificacion = document.getElementById("identificacion").value;
    if (identificacion == "") {
        alert("Campo identificacion esta vacio");
        return false;
    }
    var est = new Estudiante();

    est.nombre = nombre;
    est.matricula = matricula;
    est.identificacion =  identificacion;

    agregarEstudiante(est);

    
    /* Limpieza del formulario */ 
    limpiarFormulario(["nombre", "matricula", "identificacion"]);

}

// variable: var Storage = windows.localstorage;
// var estudiantes = [];

/* crear funcion agregarrow

fucntion agregarEstudiantes(est){
    estudiantes.push(est);

    // agregarlo a la tabla
    agrearRow(est);

    // Guardar todo en el localstorage
    Storage.setItem("estudiantes", JSON.stringify(estudiantes));
}

*/

/* Aqui definimos la funcion de agregarEstudiante() y su comportamiento con relacion a la tabla */

// con un cambio se deberia llamar agregarRow()
function agregarEstudiante(estudiante) {

    estudiantes.push(estudiante);
    storage.setItem("estudiantes", JSON.stringify(estudiantes));
    
    var tablaEstudiante = document.getElementById("tabla_estudiante");

    var tr = document.createElement("tr");

    var tdNombre = document.createElement("td");
    var tdMatricula = document.createElement("td");
    var tdIdentificacion = document.createElement("td");

    tdNombre.textContent = estudiante.nombre;
    tdMatricula.textContent = estudiante.matricula;
    tdIdentificacion.textContent = estudiante.identificacion;

    tr.appendChild(tdNombre);
    tr.appendChild(tdMatricula);
    tr.appendChild(tdIdentificacion);

    tablaEstudiante.appendChild(tr);

}
/*
function limpiarFormulario(inputs) {
    inputs.forEach(function (v, i) {
        document.getElementById(v).value = "";
    });

}*/

/* Para  definir la funcion de limpiar formulario, tambien se puede hacer de la siguiente forma */

function limpiarFormulario(){

    ["nombre","matricula","identificacion"].forEach(function(v,i){
        document.getElementById(v).value = "";
    });
    
    }

    

function cargandoTabla(estudiantes) {    
    this.estudiantes = [];
    estudiantes.forEach(
        function(v,i) {
            agregarEstudiante(v);
        }
    )  

}